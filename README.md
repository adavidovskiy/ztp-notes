[[_TOC_]]

# Notes on BitTorrent-enhanced ZTP and TKG deployment for Telco environemnts

## Introduction

Purpose of this document is to try and suggest feasible solution for installing and upgrading ESXi hosts and TKG clusters in Telco environments.
Telco environments present unique challenges that are rarely found in enterprise world:
- A lot (up to thousands) small sites
- Internet connectivity is mostly discouraged
- Customer asks for zero-touch approach for all HW and SW located on remote cell sites
- Central OS installation server / TKG image repo can be easily overwheled by sheer number of clients and amount of traffic
- There might be not enough bandwidth to push data to all required sites / machines (remember, each ESXi server or TKG node downloads same data independently)

BitTorrent technology seems like a nice answer for those challenges. Basically, it allows distributed file exchange between network of peer servers. There are already some projects that try to utilize this technology:
- [BootTorrent](https://boottorrent.readthedocs.io/en/latest/internals.html) - tool, created to network boot Linux machines using BitTorrent technology
- [Quay](https://quay.io/) - Image Repository by RedHat

While quite similar in idea, mass-installation of ESXi hosts and TKG clusters has major differences:
- ESXi hosts:
    - Require relatively small download (~300 MB)
    - Installed relatively rare
    - Utilize PXE/iPXE for automated install. Both of those options do not support BitTorrent downloads
    - Requires a lot of configuration on ESXi and vSphere level after installation
- TKG nodes:
    - Require large downloads (up to tens of gygabytes)
    - Installed quite often
    - Deployed from VMware VM templates. In theory, those templates can be easily updated to include BitTorrent Software
    - Node configuration is automatically orchestrated by TCA appliance or TKG bootstrapper

## Requirements and Scope

Having said all that, let's define functional and non-functional requirements for advanced ESXi/TKG installation techniques:
- ESXi hosts:
    - Functional Requirements:
        - MUST utilize BitTorrent to install ESXi hosts
        - MUST use nearest peers to mitigate bandwidth / security constraints
        - ESXi host SHOULD be a peer itself
    - Non-functional Requirements:
        - MUST be able to recover from situation, when there are no more reachable peers
        - MUST be able to function without internet access
        - MUST provide way to communicate info regarding installed ESXi servers to a higher level orchestration / automation system
        - MUST introduce least possible number of changes to the ESXi image and/or boot process
- TKG nodes:
    - Functional Requirements:
        - MUST utilize BitTorrent to install required packets
        - MUST use nearest peers to mitigate bandwidth / security constraints
        - MUST be peers themselves (as there are way more peers than with ESXi hosts case and they are running actual OS instead of PXE environment)
    - Non-functional Requirements:
        - MUST be able to recover from situation, when there are no more reachable peers
        - MUST be able to function without internet access

Some things, as those below, are deliberately left out of scope:
- ESXi host configuration both at ESXi and vSphere (clusters, virtual switches, etc.) level is out of scope
- DHCP service and standard PXE boot environment is expected to be present

## Telco sites architecture

Rather usual high level Telco architecture is presented as pseudo-graphics below:
- Central DC - central location with OSS/BSS and other business software running. Usually, there are couple of them.
- Regional DC - usually smaller regional DC in charge of Telco services for a region. They usually come in relatively small number - from single digit to tens
- Cell Site - closest to radio, usually only 1 or 2 servers here, but number of sites is very high - up to tens of thousands in some environments. Those are usually running majority of TKG nodes and present biggest challenge for ESXi/TKG installation & upgrade.

<img src="images/telco site hierarchy.png">

Central DC might be collapsed with Regional DC in some implementations.

***

# ESXi Installation

## Required ESXi host changes

ESXi supports network installation out of the box. As per vSphere 7 documentaion: "The network boot process varies depending on whether the target host is using legacy BIOS or UEFI firmware, and whether the boot process uses PXE TFTP, iPXE HTTP, or UEFI HTTP".
Process in general is like this: 
1. ESXi host boots and NIC PXE routine sends DHCP request
2. DHCP Server replies with leased IP address and additional information: location of TFTP (for PXE boot) or HTTP (for iPXE and Native UEFI boot) and filename of initial boot loader
3. ESXi downloads and run initial boot loader
4. Initial boot loader searches for a configuration file on the TFTP or HTTP server, downloads kernel and other ESXi components and boot kernel on the ESXi host
5. ESXi server installer runs - can be run interactively or with kickstart script. In out case of mass installation kickstart script is the only feasible option.

In order to make this process more suitable for the mass ESXi installation, we propose enhancing initial boot loader with BitTorrent functionality.
However, there is matter of managing .torrent files. As per [BitTorrent Protocol Specification](https://www.bittorrent.org/beps/bep_0003.html), .torrent file must include URL to tracker, therefore, .torrent file will be unique for each environment and cannot be hardcoded into initial boot loader.
Two options seem doable:
- Build initial boot loader that includes .torrent file as part of PXE environment installation
- Download .torrent file from TFTP/HTTP server. Location must be specified in the initial boot loader configuration file
Second option offers more flexibility (i.e. same boot loader can be used to install different versions of ESXi). Additionally, initial boot loader must be able to access TFTP/HTTP server anyway.

| Decision | Justification | Implications |
|  ---     | ---           | ----         |
| Include BitTorrent functionality into ESXi initial boot loader | It's the only feasible way to use BitTorrent during ESXi installation | - Both BIOS and UEFI boot loaders must be updated<br>- ESXi hosts will be able to participate in the file sharing during installation process<br>- .torrent file for ESXi installation files must be available on TFTP/HTTP server|
| Do not include BitTorrent functionality in the ESXi image | - It will introduce additional CPU/Memory footprint, which is expected to be minimal in Telco environments<br>- Running Torrent client might be seen as an additional attack surface by CyberSecurity| After ESXi host is installed and running, it won't participate in sharing installation files| 

Implementation might want to introduce reasonable bandwidth cap to the initial boot loader Torrent client.

## Torrent peers hierarchy

### Introduction

As was defined in [requirements section](#requirements-and-scope), ESXi hosts MUST use nearest peers to mitigate bandwidth / security constraints. Let's define, what those peers might be.
As per design decisions in [previous chapter](#required-esxi-host-changes), ESXi hosts will only participate in p2p network whem ESXi installation process is running. This means that we cannot rely on ESXi hosts alone:
- there must be multiple peers located as close to the cell sites as possible
- ESXi hosts must prefer closest peers

Following scheme can do the job for ESXi hosts on the cell sites:
- Ideally ESXi host finds a peer within cell site
- If there are no peers within site, peers from Regional DC are used
- If there are no peers in Regional DC, peers from Central DC are used

<img src="images/esxi peer hierarchy.png">

| Decision | Justification | Implications |
| --- | --- | ---|
| Introduce peer hierarchy with most preferable peers being those located in the same site | This will help to mitigate bandwidth / security constraints by keeping most of the traffic site-local| - Advanced peer-selection logic must be introduced to the BitTorrent software<br> - This kind of functionality is not usually available out of the box|
| Introduce linux-based appliances to serve as:<br> - Torrent tracker<br> - Torrent client<br> - Initial seed | As ESXi servers will seed installation files for a short period of time only, we need network of peers to distribute those files. Additionally, Torrent tracker is required to create metadata .torrent files and communicate additional metadata to peers. Last but not least, there must be means for initial upload of installation files to Torrent peer| Installation and maintenance of several linux appliances per Central/Regional DC|


Central DC contains several pieces of BitTorrent software:
- Torrent Tracker
- Initial seed
- One or more client
Implementation-wise, all those software bits might be packaged as a single appliance

Regional DCs must run at least one appliance with torrent client each. Metadata .torrent file for each actual ESXi release must be available for those peers to participate in Torrent file exchange 

### Peer Selection Option 1

Metadata .torrent file contains tracker URL, so when Torrent client gets this file, it knows were to connect.
Client sends HTTP GET Request to tracker, as defined in "Trackers" part of [BitTorrent Protocol Specification](https://www.bittorrent.org/beps/bep_0003.html)

Notably, client must create its peer_id 20 bytes value and submit it to the tracker as part of GET request. As per [Bittorrent Protocol Specification v1.0](https://wiki.theory.org/BitTorrentSpecification) peer ID is allowed to be of any value, [Peer ID Conventions](http://www.bittorrent.org/beps/bep_0020.html) define two most used Peer ID conventions. We, however, must use different convention to implement closest peer selection.
peer_id (must be 20 bytes):
- Single byte:
    - is_esxi bit. '1' if client runs within ESXi initial boot loader, '0' in any other case
    - Rest of the bits can be used for any other flags and/or peer_id spec version
- Six bytes:
    - MAC Address if client is ESXi initial boot loader (see [ZTP integration](#integration-into-ztpconfig-process) below)
    - Random value if client is not ESXi initial boot loader
- Single byte: site level in hierarchy
    - '0' - Central DC
    - '1' - Regional DC
    - '2' - Cell Site
    - It really depends on the hierarchy configuration file (see below)
- Twelve bytes: site name hash
    - Required to distinguish site from same-level peers
    - Hash required to provide fixed-size output generated from undefined-size site name
    - SHAKE128 algorithm seems like a nice way to provide such a hash

Tracker returns response with list of selected peers. Tracker keeps list of all active peers and returns to the client list of most suitable peers, based on the following logic:
- If client is not ESXi host - only non-ESXi peers are returned. That is to limit torrent traffic from Cell Sites to the sites located higher in the hierarchy
- If client is an ESXi host:
    1. If there are peers within the same site - list of those peers is returned. ESXi peers as short-lived, as Torrent client only runs within initial boot loader. This logic, however, will allow to transfer at least some of the data locally within site. As Tracker constantly updates list of active peers and Torrent client constantly query Tracker for peers, even if all ESXi peers within site cease to exist, download will resume shortly - just from different peers.
    2. If there are no active peers within site - return peers from parent site (i.e. Regional DC)
    3. If there are no active peers in parent site - query parent's parent and so on and so forth, until root-level site is reached
    4. If root-level site doesn't contain active peers - return all non-ESXi peers.

To implement such peer selection process, Tracker must have data on site hierarchy. Example below shows how this configuration file might look like. Actual look of configuration file is implementation-depended.

```
sites:
- name: Central DC                  # site level - 0
    - name: Regional DC A           # site level - 1
        - name: Cell Site A1        # site level - 2
        - name: Cell Site A2
    - name: Regional DC B           # site level - 1
        - name: Sub-Regional DC C   # site level - 2 
            - name: Cell Site C1    # site level - 3
```

### Peer Selection Option 2

Alternatively, Tracker can only report peers with IP addresses within longest possible subnet prefix of client's IP address. This will allow to easily select peers within same site, but to effectively select peers from parent site, very well-structured IP address plan must be implemented by customer.

This option is, however, much easier to implement and might be used as an MVP.
For instance, it doesn't require implementation of custom peer_id logic, defined in [previous section](#peer-selection-option-1).

### Additional considerations

Some BitTorrent implementations can discover peers not only by quering Tracker, but with different means as well. That's not the behaviour wanted for this specific usecase.

To turn it off, during medatata .torrent file generation, 'private' option within 'info' dictionary of metadata file must be set.
As [Bittorrent Protocol Specification v1.0](https://wiki.theory.org/BitTorrentSpecification) puts it:

> private: (optional) this field is an integer. If it is set to "1", the client MUST publish its presence to get other peers ONLY via the trackers explicitly described in the metainfo file. If this field is set to "0" or is not present, the client may obtain peer from other means, e.g. PEX peer exchange, dht. Here, "private" may be read as "no external peer source"

Additional info can be found in [BitTorrent Protocol Specification](https://www.bittorrent.org/beps/bep_0003.html).

## Integration into ZTP/config process

As stated in [requirements](#requirements-and-scope), solution MUST provide way to communicate info regarding installed ESXi servers to a higher level orchestration / automation system.

To do so, some ID of ESXi server must be defined. While not ideal, MAC address of NIC interface used for PXE boot, provides a feasible option.
Decision relies on the following behavior:
- MAC address of the NIC used to boot into PXE environment is known to the initial boot loader
- BitTorrent Tracker learns IP address assigned by DHCP server to this MAC
- After ESXi installation vmk0 management interface uses same NIC for communication and inherits its IP address
- DHCP server provides the same IP address to the vmk0 management interface, as it already has the lease for this MAC address

| Decision | Justification | Implications |
| --- | --- | --- |
| Use MAC address of NIC used to boot into PXE environment as ID to pass to ZTP system | MAC address is possibly single thing available both:<br> - Before shipping server to the remote site<br> - To the initial boot loader | Decision relies on default ESXi MAC-handling behavior |
| API-accessible list of ESXi hosts deployed with BitTorrent-based installation must be maintained on Tracker appliance | Tracker appliance knows all the ESXi peers | It's ZTP/configuration toolkit job to browse list and configure hosts that require configuration |

***

# TKG clusters installation


- TKG nodes:
    - Functional Requirements:
        - MUST utilize BitTorrent to install required packets
        - MUST use nearest peers to mitigate bandwidth / security constraints
        - MUST be peers themselves (as there are way more peers than ESXi hosts and they are running actual OS instead of PXE environment)
    - Non-functional Requirements:
        - MUST be able to recover from situation, when there are no more reachable peers
        - MUST be able to function without internet access

## Required TKG changes

As per [requirements](#requirements-and-scope), solution must be able to function without internet access. Telco Cloud Automation already meets this requirements by providing Airgap server functionality. 
This functionality can be further enhanced by adding BitTorrent Tracker and BitTorrent Client. Airgap Appliance msut also have means to become initial seed and generate metadata .torrent files.
Additional appliances that run BitTorrent cliens and act as peers must be also introduced. Figure below shows high-level architecture of the proposed solution.

<img src="images/tkg peer hierarchy.png">

Of course, TKG Nodes must be enhanced with ability to use BitTorrent to download both OS bits and container images.
TKG nodes must be able do work with both 'classic' and BitTorrent-enabled repositories.

## Torrent peers selection

As here are way more peers than in ESXi installation case and they are running actual OS instead of PXE environment, we can settle with way less complicated peer selection policy. Tracker can only report peers with IP addresses within longest possible subnet prefix of client's IP address, same as defined [here](#peer-selection-option-2).

Additionally, there is no need for any custom technique for peer_id creation.

## Additional considerations

Some BitTorrent implementations can discover peers not only by quering Tracker, but with different means as well. That's not the behaviour wanted for this specific usecase.

To turn it off, during medatata .torrent file generation, 'private' option within 'info' dictionary of metadata file must be set.
As [Bittorrent Protocol Specification v1.0](https://wiki.theory.org/BitTorrentSpecification) puts it:

> private: (optional) this field is an integer. If it is set to "1", the client MUST publish its presence to get other peers ONLY via the trackers explicitly described in the metainfo file. If this field is set to "0" or is not present, the client may obtain peer from other means, e.g. PEX peer exchange, dht. Here, "private" may be read as "no external peer source"

Additional info can be found in [BitTorrent Protocol Specification](https://www.bittorrent.org/beps/bep_0003.html).